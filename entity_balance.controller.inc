<?php
/**
 * @file
 * Contains Entity Balance controller class functions.
 */

/**
 * Class MwsCountController
 * Entity controller for MWS Count entity
 */
class EntityBalanceTransactionController extends EntityAPIController {

  /**
   * Provides default values for new Entity Balance Transaction entities
   * @param array $values
   * @return object
   */
  public function create(array $values = array()) {
    $values += array(
      'ebt_type'    => '',
      'entity_type' => '',
      'bundle'      => '',
      'entity_id'   => 0,
      'ebt_amount'  => 0,
      'ebt_comment' => '',
    );

    return parent::create($values);
  }
}
 