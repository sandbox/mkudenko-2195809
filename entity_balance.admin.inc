<?php
/**
 * @file
 * Contains admin interface functions for Entity Balance.
 */


function eb_settings_form($form_state) {
  $form = Array();
  $entity_types = entity_get_info();
  foreach ($entity_types as $key => $entity_type) {
    foreach ($entity_type['bundles'] as $k => $value) {
      if($k != 'entity_balance_transaction'){
        $form[$k] = array(
          '#type'         => 'checkbox',
          '#title'        => t($value['label']),
          '#return_value' => $k
        );
      }
    }
  }
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Save settings',
  );

  return $form;
}

function eb_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];
//        if() {
//            form_set_error('bundles', 'Select at least one bundle please.');
//        }
//        dpm($values);
}

function eb_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
//        dpm($values);

}